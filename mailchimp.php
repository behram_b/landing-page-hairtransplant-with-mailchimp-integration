<?php

if(isset($_POST['form_id'])){
    $form_id = $_POST['form_id'];
    $fname = $_POST['sendername-' . $form_id];
    $lname = $_POST['senderoperation-' . $form_id];
    $email = $_POST['senderemail-' . $form_id];
    $phone = $_POST['sendermessage-' . $form_id];
    $operation = $_POST['senderphone-' . $form_id];
    $message = $_POST['sendermessage-' . $form_id];

    if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false){
        // MailChimp API credentials
        $apiKey = '46abc8c999f1daeb618128d0eeff980f-us7';
        $listID = '8ad5424dea';

        $to1 = "facemailing@yandex.com";
        $to2  = "behram.b@estetikinternational.com";
        mail($to1, $operation, $message, $phone, $email);
        mail($to2, $operation, $message, $phone, $email);

        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

        // member information
        $json = json_encode([
            'email_address' => $email,
            'status'        => 'subscribed',
            'merge_fields'  => [
                'FNAME'     => $fname,
                'LNAME'     => $lname
            ]
        ]);

        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // store the status message based on response code
        if ($httpCode == 200) {
            $_SESSION['msg'] = '<p id="succes">Form başarıyla gönderildi. Kısa zamanda iletişime geçeceğiz.</p>';
        } else {
            switch ($httpCode) {
                case 214:
                    $msg = 'You are already subscribed.';
                    break;
                default:
                    $msg = '<p id="succes">Form submitted. Well be in touch shortly.</p>';
                    break;
            }
            $_SESSION['msg'] = '<p style="color: #EA4335">'.$msg.'</p>';
        }
    }else{
        $_SESSION['msg'] = '<p id="error">Please enter valid email address.</p>';
    }
}
// // redirect to homepage
// header('location:index.php');

