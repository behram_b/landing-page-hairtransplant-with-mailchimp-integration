<!DOCTYPE html>
<html lang="tr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
  <link rel="shortcut icon" href="img/icon/favicon.ico">
    <title>Estetik International Saç Ekimi </title>

  <!-- Bootstrap -->
  <link href="dist/css/bootstrap.css" rel="stylesheet">


    <style id="#jsbin-css">


    /* animations */

    @-webkit-keyframes checkmark {
    0% {
    stroke-dashoffset: 50px
    }

    100% {
    stroke-dashoffset: 0
    }
    }

    @-ms-keyframes checkmark {
    0% {
    stroke-dashoffset: 50px
    }

    100% {
    stroke-dashoffset: 0
    }
    }

    @keyframes checkmark {
    0% {
    stroke-dashoffset: 50px
    }

    100% {
    stroke-dashoffset: 0
    }
    }

    @-webkit-keyframes checkmark-circle {
    0% {
    stroke-dashoffset: 240px
    }

    100% {
    stroke-dashoffset: 480px
    }
    }

    @-ms-keyframes checkmark-circle {
    0% {
    stroke-dashoffset: 240px
    }

    100% {
    stroke-dashoffset: 480px
    }
    }

    @keyframes checkmark-circle {
    0% {
    stroke-dashoffset: 240px
    }

    100% {
    stroke-dashoffset: 480px
    }
    }

    /* other styles */
    /* .svg svg {
    display: none
    }
    */
    .inlinesvg .svg svg {
    display: inline
    }

    /* .svg img {
    display: none
    } */

    .icon--order-success svg path {
    -webkit-animation: checkmark 0.25s ease-in-out 0.7s backwards;
    animation: checkmark 0.25s ease-in-out 0.7s backwards
    }

    .icon--order-success svg circle {
    -webkit-animation: checkmark-circle 0.6s ease-in-out backwards;
    animation: checkmark-circle 0.6s ease-in-out backwards
    }
    }

    .icon.icon--order-success.svg {
    text-align: center;
    margin-top:10px;;
    }

    .icon.icon--order-success.svg h4 {
    margin-top: 10px;
    }

    ></style>




    <!-- Custom -->
  <link href="dist/css/lightbox2/dist/css/lightbox.css" rel="stylesheet">
    <?php
    $data = new stdClass();
    $data->form1_ID = isset($_GET["form1"]) ? $_GET["form1"] : 0;
    $data->form2_ID = isset($_GET["form2"]) ? $_GET["form2"] : 0;

    $data->form1_SLUG = isset($_GET["form1_slug"]) ? $_GET["form1_slug"] : 0;
    $data->form2_SLUG = isset($_GET["form2_slug"]) ? $_GET["form2_slug"] : 0;


    $data->visitor = isset($_GET["kaynak"]) ? $_GET["kaynak"] : "";
    $data->partner = isset($_GET["partner"]) ? $_GET["partner"] : false;
    ?>
          <!-- Facebook Pixel Code -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1668625600043859'); // Insert your pixel ID here.
      fbq('track', 'PageView');
      fbq('track', 'Lead');
      </script>

       <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-42271053-1', 'auto');
      ga('send', 'pageview');
    </script>

  
     <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
    try {
    w.yaCounter33369663 = new Ya.Metrika({
    id:33369663,
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true,
    trackHash:true
    });
    } catch(e) { }
    });
    var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";
    if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/33369663" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
<!-- Header -->
<nav>
  <div class="header_top navbar navbar-fixed-top">
    <div class="container">
      <div class="logo_top pull-left">
        <a href="javascript:void(0)"><img class="img-responsive" src="img/logo.png" alt="estetiklogo"></a>
      </div>
      <div class="pull-right social_bar">
        <a href="http://www.facebook.com/estetikinternational.eng" target="_blank"> <i class="fa fa-facebook"
                                                                                       aria-hidden="true"></i></a>
        <a href="http://www.instagram.com/estetikinternationaleng" target="_blank"> <i class="fa fa-instagram"
                                                                                       aria-hidden="true"></i></a>
        <a href="http://www.youtube.com/user/estetikint" target="_blank"><i class="fa fa-youtube-play"
                                                                            aria-hidden="true"></i></a>
         <p class="tel" style="  font-family: inherit !important;">444 77 07</p>
      </div>
    </div>
  </div>
</nav>

<!-- Main Left-->
<div class="col-lg-12 col-md-12 col-sm-12 main">
  <div class="container">

    <div class="row">
      <div class="hidden-xs col-xs-12 col-md-4 col-lg-offset-1">
        <p class="text-justify">Organik saç ekimi tekniğin bulucusu ödüllü Dr. Bülent CİHANTİMUR'un saç ekim merkezinde garantili kesin çözüm ile sizde mutlu olun.<br><br>
          Kendi kök hücreniz ile yeni saçlarınıza kavuşmakla kalmayın ayrıca mevcut saçlarınızın da dökülmesini engelleyin.
          <br><br> Dünyada ilk türk doktorların buluşu olan organik saç ekimi tüm dünya ile aynı anda sizlerle.</p>
      </div>

      <div id="main_world" class="hidden-xs col-xs-12 col-md-5 col-lg-offset-1">
        <iframe width="480" height="220" src="https://www.youtube.com/embed/_s63y63537M?ecver=1" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>

    <!--Main-text-mobile-->
    <div id="main_text_xs" class="hidden-sm hidden-md hidden-lg col-sm-12">
      <h1>Organik Saç Ekimi</h1>
      <ul id="main_text_xs">
        <li>Ağrısız Saç Ekimi</li>
        <li> Saç Ekimi Merkezi</li>
        <li>Sosyal Hayata Hızlı Dönüş</li>
        <li>Garantili Organik Saç Ekimi için Ücretsiz Muayene</li>
      </ul>
    </div>



    <!--Messenger icon-->
    <div class="col-md-4 col-lg-offset-1">
      <span class="cont messenger">
        <a href="https://m.facebook.com/messages/thread/909680959061460/" target="_blank">
          <img src="img/icon/fmessenger.png" width="32" height="34" alt=""></a>
      <a href="https://api.whatsapp.com/send?phone=905323783845&text=Merhaba">
        <img src="img/icon/whatsapp.png" width="30" height="30" alt=""></a>
      </span>

        <!--Form-->
        <form action="http://www.estetikinternational.com.tr/form/<?php echo $data->form1_SLUG; ?>" method="post" id="ask-dr">
            <h3>Ücretsiz Online Randevu</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text"  id="sendername-<?php echo $data->form1_ID; ?>" name="sendername-<?php echo $data->form1_ID; ?>" class="form-control" placeholder="İsim *" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="tel"  id="senderphone-<?php echo $data->form1_ID; ?>" name="senderphone-<?php echo $data->form1_ID; ?>" class="input1 form-control" placeholder="Telefon *" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="senderemail-<?php echo $data->form1_ID; ?>" name="senderemail-<?php echo $data->form1_ID; ?>" type="email" class="form-control" placeholder="Email">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input id="sendercountry-<?php echo $data->form1_ID; ?>" name="sendercountry-<?php echo $data->form1_ID; ?>" type="text" class="form-control" placeholder="Şehir">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                         <select type="text"  id="senderoperation-<?php echo $data->form1_ID; ?>" class="form-control" name="senderoperation-<?php echo $data->form1_ID; ?>" required>
                  <option value="">*Operasyon Yaptırmak İstediğiniz Zaman</option>
                  <option value="Hemen">Hemen</option> 
                  <option value="Bu Ay İçinde">Bu ay içinde</option> 
                  <option value="Gelecek Ay içinde">Gelecek ay içinde</option>
                  <option value="2 Ay İçinde">2 ay içinde</option>
                  <option value="6 ay içinde">6 ay içinde</option>
                  <option value="6 ay sonrası">6 ay sonrası</option>

              </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea name="sendermessage-<?php echo $data->form1_ID; ?>" id="sendermessage-<?php echo $data->form1_ID; ?>" class="form-control" placeholder="Mesaj" rows="4" required></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="hidden" name="visitordetail-<?php echo $data->form1_ID; ?>" value="<?php echo $data->visitor; ?>">
                    <input type="hidden" name="form_id" value="<?php echo $data->form1_ID; ?>" />
                    <input type="hidden" name="procedure-<?php echo $data->form1_ID; ?>" value="Saç Ekimi">
                    <input type="submit" class="btn btn-danger btn-send" value="Gönder">
                    <div style="display: none" class="icon icon--order-success svg">
                        <svg width="72px" height="72px">
                            <g fill="none" stroke="#8EC343" stroke-width="2">
                                <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                            </g>
                        </svg>
                        <h4 class="text-center">Form Gönderildi</h4>
                    </div>
                </div>
            </div>
        </form>
    </div>

      <!--Main_World Mobile -->

      <div id="main_world" class="visible-xs col-xs-12 col-md-5 col-lg-offset-1">
          <center><iframe width="260" height="180" src="https://www.youtube.com/embed/_s63y63537M?ecver=1&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></center>
      </div>


    <div class=" visible-xs col-xs-12 col-md-4 col-lg-offset-1">
      <p class="text-justify">Organik saç ekimi tekniğin bulucusu ödüllü Dr. Bülent CİHANTİMUR'un saç ekim merkezinde garantili kesin çözüm ile sizde mutlu olun.<br><br>
        Kendi kök hücreniz ile yeni saçlarınıza kavuşmakla kalmayın ayrıca mevcut saçlarınızın da dökülmesini engelleyin.
        <br><br>Dünyada ilk türk doktorların buluşu olan organik saç ekimi tüm dünya ile aynı anda sizlerle.</p>
    </div>

    <!--Main-Right-->
    <div id="main_text" class="hidden-sm hidden-xs col-md-6 col-lg-offset-1">
      <h1>Özel Fiyatları Kaçırmayın</h1>
      <h5>Bu fiyata sınırlı sayıdadır. Ücretsiz muayene olma imkanı.</h5>
      <h2>Saç Ekimi Merkezimizde</h2>
      <h4>Sağlığınıza Estetik Katıyoruz <br>Dört Büyük Klinik ile Profesyonel Hizmet</h4>
      <h4>Kesi Yok Ağrı Yok Sızı Yok<br>Kısa Sürede Yeni Bir Hayat</h4>
    </div>
  </div>
</div>

<!--Content-->
<div class="content">
  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-lg-offset-1">
          <a href="img/hair1.jpg" id="hair1" class="thumbnail" data-lightbox="hair1">
            <img src="img/hair1.jpg" class="cont" alt="hair1">
            <div>
              <p><b>Saç Ekimi</b><br>
                <l>estetikinternational.com.tr</l>
              </p>
              <i>
                <c>12</c>
                <br>Ay</i>
            </div>
          </a>
        </div>
        <div class="text-justify col-lg-6 col-md-6" id="hair_text">
          <p>
          <h2>Organik Saç Ekimi Nasıl Yapılır?</h2>
          Hastanın kendi yağ dokusundan alınan ve kök hücreler ile zenginleştirilen sıvı, saç ekimi yapılacak bölgeye enjekte edilir. Bu sayede hem çalışma alanı şişirilerek rahat nakil ortamı sağlanır, hem de klasik saç ekimi işlemlerinde saç köklerini zedeleyen kimyasal enjeksiyona gerek kalmaz.  Uygulama için hazır ve verimli hale getirilen saçtan yoksun bölgeye donör alandan alınan sağlıklı kıl kökleri uzman cerrahımız tarafından kolaylıkla nakledilir.

          <br><br>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-lg-offset-1">
          <a href="img/hair2.jpg" id="hair1" class="thumbnail" data-lightbox="hair2"">
          <img src="img/hair2.jpg" class="cont" alt="hair2">
          <div>
            <p><b>Saç Ekimi</b><br>
              <l>estetikinternational.com.tr</l>
            </p>
            <i>
              <c>6</c>
              <br>Ay</i>
          </div>
          </a>
        </div>

        <div id="hair_text" class="text-justify col-lg-6 col-md-6 text-justify text-center  ">
          <p>
          <h2>Organik Saç Ekiminin Avantajları</h2>
          <ul>
          <p>Organik saç ekiminde kullanılan kök hücre enjeksiyonunu sayesinde hastaya pek çok fayda sağlanır. Organik Saç Ekiminde kullanılan kök hücre enjeksiyonu:</p>
            <li>Ekim alanı verimli hale getirir</li>
            <li>Saç ekimi işlemini kolaylaştırır</li>
            <li>Kimyasal kullanmadan alanda çalışma yapılmasını sağlar</li>
            <li>Yeni transfer edilen saç köklerini besleyerek tutulma oranını artırır</li>
            <li>Saç ekimi işleminden, etkili ve verimli sonuçların alınmasını sağlar</li>
            <li>Daha canlı, sağlıklı ve uzun ömürlü saçların çıkmasını kolaylaştırır</p></li>
          </ul>





        </div>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-lg-offset-1">
            <a href="img/hair3.jpg" id="hair1" class="thumbnail" data-lightbox="hair3">
              <img src="img/hair3.jpg" class="cont" alt="hair3">

              <div>
                <p><b>Saç Ekimi</b><br>
                  <l>estetikinternational.com</l>
                </p>
                <i>
                  <c>12</c>
                  <br>Ay</i>
              </div>
            </a>
          </div>

          <div class="text-justify col-lg-6 col-md-6" id="hair_text">
          <h2>Sağlıklı, Dökülmeyen ve Gür Saçlar</h2>
            <p>açtan yoksun bölge Organik Saç Ekimi enjeksiyonu ile verimli hale getirildiğinde, cilt alt dokusunda bazı süreçler yaşanır. Dediğimiz gibi bu alanda saç dökülmesi yaşanmasının nedenlerinden birisi kemiğe yaklaşmış, saç derisidir. Bu enjeksiyon ile, öncelikle deri altının yağlanması sağlanır. <br> <br> Kök hücreler diğer yapılarla birleşerek, yeni hücrelerin yapımını hızlandırır ve cilt altı dokusunu verimli hale getirir. Bu noktada ekilen greftler, çıkış yerlerindeki sağlıklı alanda olduğu gibi, yeni bölgelerinde yaşamaya devam ederler. <br><br> Zaman içinde kök hücrenin canlandırma mekanizmasıyla daha da güçlenerek çıkan yeni saç kökleri, eskisinden daha gür ve sağlıklı saçlara kavuşulmasını sağlamaktadır.Elbette hala aklınız da hala bir çok soru olabilir bu konu da hemen bizi arayabilir ya da Saç Ekimi sayfamızı ziyaret ederek çok daha fazla bilgiye sahip olabilirsiniz. </p>
              <br> <br>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="visible-xs	 col-lg-12 col-md-12 col-sm-12">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-lg-offset-1">
<!--        <center>-->
<!--          <iframe width="300" height="230"-->
<!--                  src="https://www.youtube.com/embed/V8Ccvxs-0SI?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0"-->
<!--                  allowfullscreen></iframe>-->
<!--        </center>-->
      </div>
    </div>
  </div>
</div>
<!--</div>-->
<div class="footer">
 <!--  <a style="display: none" class="whatapp-ft" href="https://api.whatsapp.com/send?phone=905323783845&text=Merhaba">
        <i style="font-size: 27px; background-color: rgba(54, 218, 78, 0.92) " class="fa fa-whatsapp" aria-hidden="true"></i>WHATSAPP İLE SOR
    </a> -->
  <div class="socialbar_footer">
    <a href="http://www.facebook.com/estetikinternational.eng" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i></a>

    <a href="http://www.instagram.com/estetikinternationaleng" target="_blank"> <i class="fa fa-instagram" aria-hidden="true"></i></a>

    <a href="http://www.youtube.com/user/estetikint" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

  </div>
</div>


<script src="dist/css/lightbox2/dist/js/lightbox-plus-jquery.min.js"></script>
<script src="dist/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {


        var metric34 = true;

        $("#ask-dr").submit(function (e) {
            e.preventDefault();
            var $thisElement = $(this);


            
              name_ =           $("#sendername-<?php echo $data->form1_ID; ?>").val();
               phone_ =          $("#senderphone-<?php echo $data->form1_ID; ?>").val();
               email_ =           $("#senderemail-<?php echo $data->form1_ID; ?>").val();
               message_ =        $("#sendermessage-<?php echo $data->form1_ID; ?>").val();
               operation_time_ = $("#senderoperation-<?php echo $data->form1_ID; ?>").val();
               campaign_ =        $("#visitordetail-<?php echo $data->form1_ID; ?>").val();
               form_id_  =         "<?php echo $data->form1_ID; ?>";
               subject_  =          "<?php echo $data->form2_SLUG; ?>"+"- "+document.referrer;
               channel_ =         "Landing Page";
               city_ =            "<?php echo  @htmlspecialchars($_GET["city"]) ?>"

               
               
               
               

               $.ajax({
                type: "POST",
                dataType: "text",
                url : 'http://lms.estetikinternational.org/public/leads_getdata',
                //here we set the data for the post based in our form
                data : {name: name_ ,
                 phone:phone_ ,
                 email: email_ ,
                 subject: subject_,
                 operation_time: operation_time_,
                 message: message_, 
                 campaign: campaign_, 
                 form_id: form_id_,
                 channel: channel_,
                 city: city_    
             },
             success:function(data){
                alert("gönderildi")     
            },
        });


               $.ajax({
              type: 'POST',
              url: 'mailchimp.php',
              data: $thisElement.serialize(),
              success: function(veri) {
              }
              });


            $("#ask-dr input[type='submit']").attr("disabled", true);

            // if($("#sendername-<?php echo $data->form1_ID; ?>").val() == "")
            // {
            //     alert("Enter your name");
            //     $("#ask-dr input[type='submit']").attr("disabled", false);
            //     return false;
            // }

            // if($("#senderphone-<?php echo $data->form1_ID; ?>").val() == "")
            // {
            //     alert("Enter phone number");
            //     $("#ask-dr input[type='submit']").attr("disabled", false);
            //     return false;
            // }

            // if($("#senderemail-<?php echo $data->form1_ID; ?>").val() == "")
            // {
            //     alert("Enter email address");
            //     $("#ask-dr input[type='submit']").attr("disabled", false);
            //     return false;
            // }

            if($("#sendermessage-<?php echo $data->form1_ID; ?>").val() == "")
            {
                alert("Enter your message");
                $("#ask-dr input[type='submit']").attr("disabled", false);
                return false;
            }

            $("#preloader").fadeIn(250);
            <?php if($data->partner): ?>
            $.ajax({
                type: "POST",
                url: "http://partners.estetikinternational.com/api/newFormData?key=912b70ca215e3372893f51ad021bb1ed",
                data: $thisElement.serialize(),
                dataType: "json"
            });
            <?php endif; ?>
            $.ajax({
                type: "POST",
                url: "http://www.estetikinternational.com.tr/form/<?php echo $data->form1_SLUG; ?>",
                data: $thisElement.serialize(),
                dataType: "json",
                success: function(data)
                {
                    if(metric34)
                        $thisElement.append('<iframe style="display:none" src="http://www.estetikinternational.com.tr/_form_tracking.php?ia_po='+data.ID+'"></iframe>');

                    $("#sendername-<?php echo $data->form1_ID; ?>").val("");
                    $("#senderphone-<?php echo $data->form1_ID; ?>").val("");
                    $("#senderemail-<?php echo $data->form1_ID; ?>").val("");
                    $("#sendermessage-<?php echo $data->form1_ID; ?>").val("");
                    $(".icon--order-success").show();
                    setTimeout(function(){

                        $('html, body').animate({
                            scrollTop: $("body").offset().top
                        }, 2000);
                        $(".icon--order-success").hide();
                    },3000);
                    $("#preloader").fadeOut(250);
                    $("#ask-dr input[type='submit']").attr("disabled", false);

                },
                error: function(data){
                    alert("Unable to send. (X)");
                    console.log(data);
                    $("#ask-dr input[type='submit']").attr("disabled", false);
                }
            });
        });

        // $("#randevu-form").submit(function (e) {
        //     e.preventDefault();

        //     $("#randevu-form input[type='submit']").attr("disabled", true);

        //     var $thisElement = $(this);

        //     if($("#alt_sendername-<?php echo $data->form2_ID; ?>").val() == "")
        //     {
        //         alert("Enter your name");
        //         $("#randevu-form input[type='submit']").attr("disabled", false);
        //         return false;
        //     }
        //     if($("#alt_senderphone-<?php echo $data->form2_ID; ?>").val() == "")
        //     {
        //         alert("Enter phone number");
        //         $("#randevu-form input[type='submit']").attr("disabled", false);
        //         return false;
        //     }
        //     // if($("#alt_senderemail-<?php echo $data->form2_ID; ?>").val() == "")
        //     // {
        //     //     alert("Enter email address");
        //     //     $("#randevu-form input[type='submit']").attr("disabled", false);
        //     //     return false;
        //     // }
        //     if($("#alt_sendermessage-<?php echo $data->form2_ID; ?>").val() == "")
        //     {
        //         alert("Enter your message");
        //         $("#randevu-form input[type='submit']").attr("disabled", false);
        //         return false;
        //     }

        //     $("#preloader").fadeIn(250);


        //     <?php if($data->partner): ?>
        //     $.ajax({
        //         type: "POST",
        //         url: "http://partners.estetikinternational.com/api/newFormData?key=912b70ca215e3372893f51ad021bb1ed",
        //         data: $thisElement.serialize(),
        //         dataType: "json"
        //     });
        //     <?php endif; ?>



        //     $.ajax({
        //         type: "POST",
        //         url: "http://www.estetikinternational.com/form/<?php echo $data->form2_SLUG; ?>",
        //         data: $thisElement.serialize(),
        //         dataType: "json",
        //         success: function(data)
        //         {
        //             if(metric34)
        //                 $thisElement.append('<iframe style="display:none" src="http://www.estetikinternational.com/form_tracking.php?ia_po='+data.ID+'"></iframe>');


        //             $(".icon--order-success").show();
        //             $("#preloader").fadeOut(250);
        //             $("#alt_sendername-<?php echo $data->form2_ID; ?>").val("");
        //             $("#alt_senderphone-<?php echo $data->form2_ID; ?>").val("");
        //             $("#alt_senderemail-<?php echo $data->form2_ID; ?>").val("");
        //             $("#alt_sendermessage-<?php echo $data->form2_ID; ?>").val("");

        //             $("#randevu-form input[type='submit']").attr("disabled", false);



        //         }, error: function(data){
        //             console.log(data);
        //             $("#randevu-form").append("<p style='float:right; color:#f00;'><strong>Unable to send. (X)</strong></p>");
        //             $("#preloader").fadeOut(250);

        //             $("#randevu-form input[type='submit']").attr("disabled", false);


        //         }
        //     });
        // })

    });
</script>

<script>
    function PathLoader(el) {
        this.el = el;
        this.strokeLength = el.getTotalLength();


        // set dash offset to 0
        this.el.style.strokeDasharray =
            this.el.style.strokeDashoffset = this.strokeLength;
    }

    PathLoader.prototype._draw = function (val) {
        this.el.style.strokeDashoffset = this.strokeLength * (1 - val);
    }

    PathLoader.prototype.setProgress = function (val, cb) {
        this._draw(val);
        if(cb && typeof cb === 'function') cb();
    }

    PathLoader.prototype.setProgressFn = function (fn) {
        if(typeof fn === 'function') fn(this);
    }

    var body = document.body,
        svg = document.querySelector('svg path');

    if(svg !== null) {
        svg = new PathLoader(svg);

        setTimeout(function () {
            document.body.classList.add('active');
            svg.setProgress(1);
        }, 200);
    }

    document.addEventListener('data == success', function () {

        if(document.body.classList.contains('active')) {
            document.body.classList.remove('active');
            svg.setProgress(0);
            return;
        }
        document.body.classList.add('active');
        svg.setProgress(1);
    });
</script>
<script id="jsbin-source-css" type="text/css">/* animations */

    @-webkit-keyframes checkmark {
        0% {
            stroke-dashoffset: 50px
        }

        100% {
            stroke-dashoffset: 0
        }
    }

    @-ms-keyframes checkmark {
        0% {
            stroke-dashoffset: 50px
        }

        100% {
            stroke-dashoffset: 0
        }
    }

    @keyframes checkmark {
        0% {
            stroke-dashoffset: 50px
        }

        100% {
            stroke-dashoffset: 0
        }
    }

    @-webkit-keyframes checkmark-circle {
        0% {
            stroke-dashoffset: 240px
        }

        100% {
            stroke-dashoffset: 480px
        }
    }

    @-ms-keyframes checkmark-circle {
        0% {
            stroke-dashoffset: 240px
        }

        100% {
            stroke-dashoffset: 480px
        }
    }

    @keyframes checkmark-circle {
        0% {
            stroke-dashoffset: 240px
        }

        100% {
            stroke-dashoffset: 480px
        }
    }

    /* other styles */
    /* .svg svg {
        display: none
    }
     */
    .inlinesvg .svg svg {
        display: inline
    }

    /* .svg img {
        display: none
    } */

    .icon--order-success svg path {
        -webkit-animation: checkmark 0.25s ease-in-out 0.7s backwards;
        animation: checkmark 0.25s ease-in-out 0.7s backwards
    }

    .icon--order-success svg circle {
        -webkit-animation: checkmark-circle 0.6s ease-in-out backwards;
        animation: checkmark-circle 0.6s ease-in-out backwards
    }

</script>
<script>
    $(document).ready(function () {
        $(".input1").keyup(function (){
            if (this.value.match(/[^0-9]/g)){
                this.value = this.value.replace(/[^0-9]/g,'');
            }
        });
    });

</script>
</body>
</html>
